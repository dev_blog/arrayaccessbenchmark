﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class Benchmark : MonoBehaviour
{
	private const int ARRAY_SIZE = 10000;
	private const int ITERATIONS = 10000;

	[SerializeField] private Text text;

	// Use this for initialization
	void Start ()
	{
		var array = new TestClass[ARRAY_SIZE];
		for (var i = 0; i < ARRAY_SIZE; ++i)
		{
			array[i] = new TestClass();
		}

		var timeSpent = new long[3];

		var sb = new StringBuilder(1000);
		sb.AppendLine($"Iterating {ITERATIONS} times over Array size {ARRAY_SIZE}");
		sb.AppendLine();
		
		var stopwatch = new Stopwatch();
		stopwatch.Start();
		{

			for (var i = 0; i < ITERATIONS; ++i)
			{
				IterateWithoutBoundsAndNullChecks(array);
			}

			timeSpent[0] = stopwatch.ElapsedMilliseconds;
		}

		stopwatch.Restart();
		{

			for (var i = 0; i < ITERATIONS; ++i)
			{
				IterateWithoutBounds(array);
			}

			timeSpent[1] = stopwatch.ElapsedMilliseconds;
		}

		stopwatch.Restart();
		{

			for (var i = 0; i < ITERATIONS; ++i)
			{
				IterateWithBounds(array);
			}

			timeSpent[2] = stopwatch.ElapsedMilliseconds;
		}

		sb.AppendLine("Array iteration: no bounds and no null checks " + timeSpent[0]);
		sb.AppendLine("Array iteration: no bounds check " + timeSpent[1]);
		sb.AppendLine("Array iteration: with bounds and null check " + timeSpent[2]);
		
		text.text = sb.ToString();

		Debug.LogWarning(array[0].v);
	}

	[Il2CppSetOption(Option.ArrayBoundsChecks, true)]
	[Il2CppSetOption(Option.NullChecks, true)]
	private void IterateWithBounds(TestClass[] array)
	{
		for (var i = 0; i < array.Length; ++i)
		{
			array[i].Test();
		}
	}
	
	[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
	[Il2CppSetOption(Option.NullChecks, true)]
	private void IterateWithoutBounds(TestClass[] array)
	{
		for (var i = 0; i < array.Length; ++i)
		{
			array[i].Test();
		}
	}
	
	[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
	[Il2CppSetOption(Option.NullChecks, false)]
	private void IterateWithoutBoundsAndNullChecks(TestClass[] array)
	{
		for (var i = 0; i < array.Length; ++i)
		{
			array[i].Test();
		}
	}

	private class TestClass
	{
		public int v;
		
		public void Test()
		{
			v += 1;
		}
	}
}
